package uq.deco2800.pyramidscheme.chat;

import org.junit.Assert;
import org.junit.Test;
import uq.deco2800.singularity.common.representations.realtime.IncomingMessage;

public class ChatMessageTest {
	private final String text = "message text";
	private final String channelId = "channelId1";
	private final String username = "username";
	private final String authorId = "userId1";
	private final String author = "authorUsername";

	private void assertExpectedChatMessage(ChatMessage m) {
		Assert.assertEquals(text, m.message);
		Assert.assertEquals(channelId, m.channelId);
		Assert.assertEquals(username, m.selfUsername);
		Assert.assertEquals(authorId, m.authorUserId);
		Assert.assertEquals(author, m.authorUsername);
	}

	@Test
	public void chatMessageTest() {
		ChatMessage m = new ChatMessage(text, channelId, username, authorId, author);

		assertExpectedChatMessage(m);
	}

	@Test
	public void chatMessageFromIncomingTest() {
		IncomingMessage incoming = new IncomingMessage();
		incoming.setMessage(text);
		incoming.setToThreadId(channelId);
		incoming.setFromUserId(authorId);
		incoming.setFromUserName(author);

		ChatMessage m = new ChatMessage(incoming, username);

		assertExpectedChatMessage(m);
	}
}
