package uq.deco2800.pyramidscheme.chat;

import javafx.collections.ObservableList;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class ChatStoreTest {
	private final static int numChannels = 10;
	private final static int numMessages = 10;

	private static List<String> channelIds;
	private static Map<String, List<ChatMessage>> map;

	/**
	 * Generates the given number of channel Ids
	 */
	private static List<String> generateChannelIds(int numChannels) {
		final String channelIdFormat = "channel id %d";

		return IntStream.range(0, numChannels)
				.mapToObj(i -> String.format(channelIdFormat, i))
				.collect(Collectors.toList());
	}
	/**
	 * Generates messages for the given channel Id
	 */
	private static List<ChatMessage> generateMessagesForChannel(String channelId, int numMessages) {
		final String messageTextFormat = "message text %d";
		final String authorUserIdFormat = "userId %d";
		final String authorUsernameFormat = "authorUsername %d";
		final String username = "username1";

		return IntStream.range(0, numMessages)
				.mapToObj(i -> {
					String messageText = String.format(messageTextFormat, i);
					String authorUserId = String.format(authorUserIdFormat, i);
					String authorUsername = String.format(authorUsernameFormat, i);

					return new ChatMessage(messageText, channelId, username, authorUserId, authorUsername);
				})
				.collect(Collectors.toList());
	}

	@BeforeClass
	public static void generateData() {
		/* Generate the channel ids and messages */
		channelIds = generateChannelIds(numChannels);

		map = new HashMap<>();
		for (String channelId : channelIds) {
			List<ChatMessage> messages = generateMessagesForChannel(channelId, numMessages);
			map.put(channelId, messages);
		}
	}

	@Test
	public void addMessage() {
		/* Populate store */
		ChatStore chatStore = new ChatStore();
		channelIds.forEach(channelId -> map.get(channelId).forEach(chatStore::addMessage));

		/* Check the messages can be retrieved from the store */
		channelIds.forEach(channelId -> {
			List<ChatMessage> messages = chatStore.getMessagesForChannel(channelId);
			assertEquals(map.get(channelId), messages);
		});
	}

	@Test
	public void addChannel() {
		List<ChatChannel> channels = channelIds.stream()
				.map(channelId -> new ChatChannel(channelId, String.format("Channel %s", channelId)))
				.collect(Collectors.toList());

		/* Populate store */
		ChatStore chatStore = new ChatStore();
		channels.forEach(chatStore::addChannel);

		/* Check the channels can be retrieved from the store */
		assertEquals(channels, chatStore.getChannels());
	}

	@Test
	public void removeChannel() {
		List<ChatChannel> channels = channelIds.stream()
				.map(channelId -> new ChatChannel(channelId, String.format("Channel %s", channelId)))
				.collect(Collectors.toList());

		/* Populate store */
		ChatStore chatStore = new ChatStore();
		channels.forEach(chatStore::addChannel);

		int size = chatStore.getChannels().size();

		chatStore.removeChannel(channels.get(0));

		Assert.assertTrue(chatStore.getChannels().size() == size - 1);
	}
}