package uq.deco2800.pyramidscheme.chat;

/**
 * Immutable representation of a channel.
 */
public class ChatChannel {
    public final String channelId;
    public final String channelName;

    public ChatChannel(String channelId, String channelName) {
        this.channelId = channelId;
        this.channelName = channelName;
    }

    @Override
    public String toString() {
        return channelName;
    }
}
